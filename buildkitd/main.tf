resource "helm_release" "this" {
  chart     = "https://gitlab.wikimedia.org/repos/releng/buildkit-chart/-/jobs/artifacts/main/raw/wmf/charts/buildkitd.tgz?job=package-and-publish-helm-chart"
  name      = "buildkitd"
  namespace = var.namespace

  values = [
    templatefile("${path.module}/buildkitd-values.yaml.tftpl", {
      buildkitd_tag          = var.buildkitd_tag
      max_connections        = var.max_connections
      docker_hub_mirror_fqdn = var.docker_hub_mirror_fqdn
      cpu_request            = var.cpu_request
      memory_request         = var.memory_request
      memory_limit           = var.memory_limit
      pvc_enabled            = var.pvc_enabled
      pvc_size               = var.pvc_size
      gckeepstorage_bytes    = var.gckeepstorage * pow(10,9)
    })
  ]
}

