variable "buildkitd_tag" {
  type = string
}
variable "max_connections" {
  description = "Maximum number of concurrent connections to each buildkitd instance"
  type        = number
}

variable "docker_hub_mirror_fqdn" {
  type = string
}

variable "namespace" {
  type = string
}

variable "cpu_request" {
  type = string
}

variable "memory_request" {
  type = string
}

variable "memory_limit" {
  type = string
}

variable "pvc_enabled" {
  type = bool
}

variable "pvc_size" {
  type = string
}

variable "gckeepstorage" {
  type = number
  description = "BuildkitD cache target storage size in GB"
}
