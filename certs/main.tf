# The namespace for the cert-manager controller
resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "certs"
    }

    name = "certs"
  }
}

resource "helm_release" "this" {
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  name       = "cert-manager"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "v1.10.1"

  set {
    name  = "installCRDs"
    value = "true"
  }
}

resource "helm_release" "issuers" {
  # T359661: Ensure the CRD for ClusterIssuer is installed before attempting to
  # install this chart which uses it.  
  depends_on = [
    helm_release.this
  ]
  name  = "cert-manager-issuers"
  chart = "${path.module}/issuers"
}

output "depend" {
  value = helm_release.issuers.metadata[0].name
  description = "This output is to allow for establishing dependencies"
}
