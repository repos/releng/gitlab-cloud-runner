resource "helm_release" "this" {
  chart     = "https://gitlab.wikimedia.org/repos/releng/reggie/-/jobs/artifacts/main/raw/helm/charts/reggie.tgz?job=package-and-publish-helm-chart"
  name      = "reggie"
  namespace = var.namespace

  values = [
    templatefile("${path.module}/reggie-values.yaml.tftpl", {
      container_registry_fqdn   = var.container_registry_fqdn
      cpu_request               = var.cpu_request
      memory_request            = var.memory_request
      memory_limit              = var.memory_limit
      ephemeral_storage_request = var.ephemeral_storage_request
      # This value is not used by the template.  It is just here
      # to establish dependencies within Terraform.
      dependencies              = var.dependencies
    })
  ]
}
