variable "container_registry_fqdn" {
  type = string
}

variable "namespace" {
  type = string
}

variable "cpu_request" {
  type = string
}

variable "memory_request" {
  type = string
}

variable "memory_limit" {
  type = string
}

variable "ephemeral_storage_request" {
  type = string
}

variable "dependencies" {
  type = list
}
