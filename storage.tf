# Give gitlab-runner access to the DO Spaces bucket for its cache.
# (See gitlab-runner/values.yaml)
resource "kubernetes_secret" "s3access" {
  metadata {
    name = "s3access"
    namespace = "gitlab-runner"
  }

  data = {
    accesskey = var.access_id
    secretkey = var.secret_key
  }
}

# Give buildkitd access to the DO Spaces bucket for its cache.
# (See buildkitd/values.yaml)
resource "kubernetes_secret" "buildkit_s3access" {
  metadata {
    name = "buildkit-s3access"
    namespace = "gitlab-runner"
  }

  data = {
    access_id = var.access_id
    secret_key = var.secret_key
    region = var.bucket_region
    bucket = var.cache_bucket
  }
}
