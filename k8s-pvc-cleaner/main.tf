resource "helm_release" "this" {
  chart     = "https://gitlab.wikimedia.org/repos/releng/k8s-pvc-cleaner/-/jobs/artifacts/main/raw/helm/charts/pvc-cleaner.tgz?job=package-and-publish-helm-chart"
  name      = "pvc-cleaner"
  namespace = var.namespace

  values = [
    templatefile("${path.module}/k8s-pvc-cleaner-values.yaml.tftpl", 
    {}
   )
  ]
}
