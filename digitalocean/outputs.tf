output "cluster_endpoint" {
  value = digitalocean_kubernetes_cluster.cloud_runner.endpoint
}

output "cluster_token" {
  value     = digitalocean_kubernetes_cluster.cloud_runner.kube_config[0].token
  sensitive = true
}

output "cluster_ca_certificate" {
  value = base64decode(
    digitalocean_kubernetes_cluster.cloud_runner.kube_config[0].cluster_ca_certificate
  )
}

output "cluster_subnet" {
  value       = digitalocean_kubernetes_cluster.cloud_runner.cluster_subnet
  description = "The subnet range from which IPs are allocated to pod endpoints"
}

output "cluster_service_subnet" {
  value       = digitalocean_kubernetes_cluster.cloud_runner.service_subnet
  description = "The subnet range from which IPs are allocated to service endpoints"
}

output "container_registry_fqdn" {
  value       = "registry.${var.cluster_domain}"
  description = "The FQDN of reggie's load balancer"
}

output "docker_hub_mirror_fqdn" {
  value       = "docker-hub-mirror.${var.cluster_domain}"
  description = "The FQDN of the docker hub mirror"
}

output "kube_config" {
  value = digitalocean_kubernetes_cluster.cloud_runner.kube_config[0].raw_config
  description = "The raw kubeconfig for the Kubernetes cluster"
}
