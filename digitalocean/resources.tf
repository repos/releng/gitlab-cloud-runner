resource "digitalocean_project" "gitlab_cloud_runner" {
  name        = var.project
  description = "A project for our GitLab CI cloud runners (https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner)"
  resources = [
    digitalocean_kubernetes_cluster.cloud_runner.urn,
    digitalocean_domain.cluster_domain.urn,
    digitalocean_spaces_bucket.runner_cache.urn,
  ]
}

resource "digitalocean_kubernetes_cluster" "cloud_runner" {
  name         = var.cluster_name
  region       = var.cluster_region
  ha           = var.cluster_ha
  version      = var.kubernetes_version           
  auto_upgrade = var.cluster_auto_upgrade

  maintenance_policy {
    start_time = "04:00"
    day        = "sunday"
  }

  # Default node pool for the cluster to run things like kube operators and
  # monitoring. Additional node pools for specific CI workloads are defined
  # below
  node_pool {
    name = "cloud-runner-pool"
    size       = var.node_pool_general_type
    auto_scale = true
    min_nodes  = 2
    max_nodes  = 4
  }
}

# Define an additional memory optimized node pool for memory-bound CI workloads
resource "digitalocean_kubernetes_node_pool" "memory_optimized_pool" {
  cluster_id = digitalocean_kubernetes_cluster.cloud_runner.id

  name       = "memory-optimized"
  size       = var.node_pool_memory_optimized_type
  auto_scale = true
  min_nodes  = var.node_pool_min_nodes
  max_nodes  = var.node_pool_max_nodes

  labels = {
    node-optimized-for = "memory"
  }
}

# DNS domain
resource "digitalocean_domain" "cluster_domain" {
  name = var.cluster_domain
}

# Create a DO Spaces bucket for runner and buildkitd caches
resource "digitalocean_spaces_bucket" "runner_cache" {
  name   = var.cache_bucket
  region = var.bucket_region
  force_destroy = true
}
