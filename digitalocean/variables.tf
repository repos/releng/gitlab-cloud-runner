variable "project" {
  description = "The project name under which to create resources"
  type        = string
}

variable "access_id" {
  description = "The access key ID used for Spaces API operations."
}

variable "secret_key" {
  description = "The secret access key used for Spaces API operations."
  type        = string
  sensitive   = true
}

variable "cache_bucket" {
  description = "Bucket used for caches"
  type        = string
  default     = "org-wikimedia-releng-cloud-runner-cache"
}

variable "bucket_region" {
  description = "Region of the spaces storage bucket"
  type        = string
  default     = "nyc3"
}

variable "cluster_region" {
  description = "Region of Kubernetes cluster"
  type        = string
  default     = "nyc3"
}

variable "cluster_ha" {
  description = "Switch to enable high available control plane"
  type        = bool
  default     = false
}

variable "cluster_auto_upgrade" {
  description = "Switch to enable automatic patch release upgrades"
  type        = bool
  default     = false
}

variable "cluster_name" {
  description = "name for the cluster"
  type        = string
  default     = "cloud-runner"
}

variable "cluster_domain" {
  description = "Domain name for the cluster"
  type        = string
  default     = "cloud.releng.team"
}

variable "container_registry_hostname" {
  description = "Host name of reggie's load balancer"
  type        = string
  default     = "registry"
}

variable "docker_hub_mirror_hostname" {
  description = "Host name of the docker hub mirror"
  type        = string
  default     = "docker-hub-mirror"
}

variable "node_pool_min_nodes" {
  description = "Minimum number of machine size in each Kubernetes node pool"
  type        = number
  default     = 4
}

variable "node_pool_max_nodes" {
  description = "Maximum number of machine size in each Kubernetes node pool"
  type        = number
  default     = 20
}

variable "node_pool_general_type" {
  description = "Instance type used for general purpose nodes"
  type        = string
}

variable "node_pool_memory_optimized_type" {
  description = "Instance type used for memory optimized nodes"
  type        = string
}

 variable "kubernetes_version" {
   description = "The kubernetes version in use."
   type        = string
 }
