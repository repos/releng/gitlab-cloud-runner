# The namespace for the ingress-nginx controller
resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "ingress"
    }

    name = "ingress"
  }
}

resource "helm_release" "this" {
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  name       = "ingress-nginx"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "4.11.2"

  values = [
    templatefile("${path.module}/values/ingress-nginx.yaml.tftpl", {
      cluster_subnet = var.cluster_subnet
      cluster_service_subnet = var.cluster_service_subnet
      cluster_domain = var.cluster_domain
      service_cluster_ip = var.service_cluster_ip
      # This value is not used by the template.  It is just here
      # to establish dependencies within Terraform.
      dependencies = var.dependencies
    })
  ]
}

output "depend" {
  value = helm_release.this.metadata[0].name
  description = "This output is to allow for establishing dependencies"
}
