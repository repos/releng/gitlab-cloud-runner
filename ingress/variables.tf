variable "cluster_domain" {
  description = "Domain name for the cluster"
  type        = string
}

variable "service_cluster_ip" {
  description = "ClusterIP for the nginx service"
  type        = string
}

variable "cluster_subnet" {
  type        = string
  description = "The subnet range from which IPs are allocated to pod endpoints"
}

variable "cluster_service_subnet" {
  type        = string
  description = "The subnet range from which IPs are allocated to service endpoints"
}

variable "dependencies" {
  type        = list
}
