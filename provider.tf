terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.14"
    }
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
  }
  backend "http" {
  }
}

provider "digitalocean" {
  spaces_access_id  = var.access_id
  spaces_secret_key = var.secret_key
}

provider "kubernetes" {
  host                   = module.do.cluster_endpoint
  token                  = module.do.cluster_token
  cluster_ca_certificate = module.do.cluster_ca_certificate
}

provider "helm" {
  kubernetes {
    host                   = module.do.cluster_endpoint
    token                  = module.do.cluster_token
    cluster_ca_certificate = module.do.cluster_ca_certificate
  }
}
