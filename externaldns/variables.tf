variable "namespace" {
  type = string
}
variable "cluster_name" {
  type = string
}
variable "token" {
  description = "A DigitalOcean API token with read/write access for updating DNS records"
  type = string
  sensitive = true
}
