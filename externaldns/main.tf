resource "helm_release" "external-dns" {
  chart     = "external-dns"
  name      = "external-dns"
  repository = "https://kubernetes-sigs.github.io/external-dns"
  version    = "1.13.1"
  namespace = var.namespace

  values = [
    templatefile("${path.module}/external-dns-values.yaml.tftpl", {
      cluster_name = var.cluster_name
    })
  ]
}

resource "kubernetes_secret" "external-dns" {
  metadata {
    name = "external-dns"
    namespace = var.namespace
  }

  data = {
    token = var.token
  }
}

