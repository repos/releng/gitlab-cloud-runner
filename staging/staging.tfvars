project = "gitlab-cloud-runner-staging"
cache_bucket = "org-wikimedia-releng-cloud-runner-staging-cache"
cluster_domain = "staging.cloud.releng.team"
node_pool_min_nodes = 1
node_pool_max_nodes = 20
cluster_name = "staging-cloud-runner"

# Downsize node types and resource requests to save on costs
#
# DO cannot change instance size for existing node pools:
# See https://github.com/digitalocean/terraform-provider-digitalocean/issues/558
# Must run terraform destroy before deploying when changing this value.
node_pool_general_type = "s-2vcpu-4gb"
node_pool_memory_optimized_type = "g-8vcpu-32gb"

# Service resource requests and limits, and other settings
buildkitd_tag = "wmf-v0.20.0-2"
buildkitd_pvc_enabled = true
buildkitd_pvc_size = "8Gi"
buildkitd_gckeepstorage = 4
buildkitd_cpu_request = "200m"
buildkitd_memory_request = "200Mi"
buildkitd_memory_limit = "800Mi"

docker_hub_mirror_pvc_size = "10Gi"

reggie_cpu_request = "200m"
reggie_ephemeral_storage_request = "1Gi"
reggie_memory_request = "200Mi"
reggie_memory_limit = "800Mi"

runner_cpuopt_cpu_request = "100m"
runner_cpuopt_cpu_limit = ""
runner_cpuopt_memory_request = "500Mi"
runner_cpuopt_memory_limit = "500Mi"

# The goal is to allow 3 jobs to run comfortably on a memory-
# optimized node.

# s-8vcpu-16gb has 7900m allocatable CPU.
# System pods request about 510m.  Let's say that 7200M cpu is available for runners.
# Per runner overhead:
# istio-proxy adds cpu request of 100m and mem of 128Mi.
# the helper  adds cpu request of 100m and mem of 512Mi (See ../gitlab/gitlab-runner-values.yaml.tftpl)
# 7200/3 = 2400 cpu available per job.  Removing 200m for overhead and
# another 100m for margin, land on 2100m request.
runner_memopt_cpu_request = "2100m"
runner_memopt_cpu_limit = ""
# s-8vcpu-16gb has 13862Mi allocatable memory.
# System pods request about 580Mi.  Let's say that 13200Mi is available for runners.
# Per runner overhead:
# istio-proxy adds cpu request of 100m and mem of 128Mi.
# the helper  adds cpu request of 100m and mem of 512Mi (See ../gitlab/gitlab-runner-values.yaml.tftpl)
# 13200/3 = 4400Mi mem available per job.  Removing 640Mi for overhead
# and another 60Mi for margin, land on 3700Mi request.
runner_memopt_memory_request = "3700Mi"
runner_memopt_memory_limit = "3700Mi"
kubernetes_version = "1.30.10-do.0"
