# Gitlab Cloud Runner

Repository for configuration of general purpose GitLab Cloud Runner.

## Provisioning of infrastructure

Terraform is used to provision the infrastructure and deploy to the cluster. Terraform state is managed by GitLab. `.gitlab-ci.yml` has CI jobs to configure the Terraform state, validate the code, plan/diff and deploy the code to the cloud provider.

There are optional jobs:
* `destroy-staging` and `destroy-production` - runs `terraform destroy` to remove the infrastructure.
* `push-to-production` - pushes to the production branch and deploys to the production cluster
* `deploy-staging` -  deploys to the staging cluster
* `load-test` - runs load tests in production

Most configuration parameters are exposed using Terraform variables in `production/variables.tf` and `staging/variables.tf`. The default values are defined in `variables.tf`.

## Configuration of stack for Kubernetes Runner/Executor

`digitalocean` contains the terraform configuration for digitalocean resources such as project, storage bucket, and kubernetes cluster where the runner will be deployed.

`gitlab` contains the terraform configuration and helm values file for the `gitlab/gitlab-runner` helm chart, which deploys the GitLab Kubernetes Executor to the cluster. 

`buildkitd` contains the terraform configuration and helm values file for the `releng/buildkit` helm chart, which deploys the buildkitd instances to the cluster.

`docker-hub-mirror` contains the terraform configuration and helm values file for the `releng/docker-hub-mirror` helm chart, which provides a read-only pull-through mirror of the docker.io registry to avoid the pull limits imposed by docker.io

`reggie` contains the terraform configuration and helm values file for the `releng/reggie` helm chart, which deploys a container registry for use by the gitlab runner.

`certs` contains the terraform configuration and helm values file for the `jetstack.io/cert-manager` helm chart, which provides SSL certs for reggie and docker-hub-mirror.

`ingress` contains the terraform configuration and helm values file for the `kubernetes.github.io/ingress-nginx` helm chart, which provides a load balancer for external access to services in the cluster.

`monitoring` contains the terraform configuration and helm values file for the `prometheus-community.github.io/helm-charts/helm-prometheus-stack` helm chart, which provides metrics collection for use by istio and other services.

`istio` contains the terraform configuration and helm values file for the `istio-release.storage.googleapis.com/charts` istio charts, which provides traffic management (consistent hash load balancing, and limiting concurrent connections) of buildkitd, and exposes metrics on concurrent connections.

`externaldns` contains the terraform configuration and helm values for the https://kubernetes-sigs.github.io/external-dns chart which maintains DNS records for registry.<cluster_domain> and docker-hub-mirror.<cluster_domain>. 

`.gitlab-ci.yml` contains CI jobs to plan and apply the helm files as terraform resources.

## How to setup the environment

 * Create a Digital Ocean API token and save it as `DIGITALOCEAN_TOKEN` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected)
 * Create a runner registration token and save it as  `TF_VAR_runner_token` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected). Do the same for `RUNNER_TOKEN_MEM_OPTIMIZED`
 * Do the same for `RUNNER_TOKEN_STAGING` and `RUNNER_TOKEN_STAGING_MEM_OPTIMIZED` for the staging cluster
 * Create a Maintainer project access token with push access and save it as `DEPLOY_TOKEN` under [Settings > Access Tokens](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/access_tokens)
 * Create a Digital Ocean spaces access key and save the id as `TF_VAR_access_id` and the key as `TF_VAR_secret_key` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected)
 * Create a docker hub account and save the username as `TF_VAR_docker_hub_user` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd)
 * Save the docker hub password as `TF_VAR_docker_hub_password` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected)
 * Save the desired maximum buildkitd connections as `TF_VAR_buildkitd_max_connections` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd)
 * Run the pipeline on the main branch to run `terraform plan`. A failure to plan will block deploy to staging and push to production.
 * Manually run the deploy-staging job to deploy to the staging cluster
 * Run the pipeline on the production branch to deploy to production
 * Check GitLab Admin area for new Runner

## How to increase the size of the PVCs (for example for buildkit or docker-hub-mirror)

Buildkitd and docker-hub-mirror each are deployed as a
[statefulset](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)
which contains a persistent volume claim (PVC) template.  The
persistent volume is used to hold files that are used by the
particular system (e.g, buildkitd's build cache or docker-hub-mirror's
image cache).

Even though the size of an existing PVC can be increased, changing the
value for the size of the PVC in the Statefulset template is not
allowed by Kubernetes.  To work around this, perform the following
steps:

1. Ensure that your environment (such as `KUBECONFIG` variable) is configured so that running
`kubectl` will reach the desired Kubernetes cluster.
1. Resize the existing PVCs by running `scripts/resize-pvc.py STATEFULSET SIZE`,
replacing `STATEFULSET` with the same of the statefulset whose PVCs you want to resize
and `SIZE` with the desired new size, in usual Kubernetes syntax.  For example:
    ```
    $ scripts/resize-pvc.py buildkitd 40Gi
    Using kubectl context "do-nyc3-cloud-runner"

    The following PVCs will be affected:
    buildkitd-storage-buildkitd-0, Req: 30Gi, Cur: 30Gi
    buildkitd-storage-buildkitd-1, Req: 30Gi, Cur: 30Gi
    buildkitd-storage-buildkitd-2, Req: 30Gi, Cur: 30Gi
    buildkitd-storage-buildkitd-3, Req: 30Gi, Cur: 20Gi
    Resize these PVCs to 40Gi? 
    ```
1. Change the value of the PVC size in the terraform configuration to
match the new size.  For buildkitd, change `buildkitd_pvc_size` in
`production/prod.tfvars` and/or `staging/staging.tfvars`.  For
docker-hub-mirror, change `docker_hub_mirror_pvc_size` in
`production/prod.tfvars` and/or `staging/staging.tfvars`.
1. Commit the changes and push up to GitLab for code review.
1. Merge the commit.
1. Delete the existing Statefulset from the cluster, orphaning the
resources that it has created.  This leaves existing pods
running.
    ```
    kubectl -n gitlab-runner delete statefulset --cascade=orphan NAME
    ```
NAME will be `buildkitd` or `docker-hub-mirror`.
1. Go through the usual deployment steps.  This should recreate the
Statefulset which will continue to use the existing PVCs.

## How to destroy the staging cluster

1. Visit https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/pipelines and click on the blue `Run pipeline` button.  
1. Add variable `CONFIRM_DESTROY` with the value `gitlab-cloud-runner`.
1. Click the `Run pipeline` button.
1. Eventually the pipeline will stop, waiting for you to manually start the `destroy-staging` job.  Start the job.
1. If destruction is successful, the pipeline will stop waiting for you to manually start the `destroy-production` job.  At this point you can cancel the pipeline.

### Preparing to rebuild the staging cluster

Change the value of `cache_bucket` in `staging/staging.tfvars`, commit, and push to GitLab. This is to work around an issue with DigitalOcean Spaces where if a Space is deleted, its name cannot be reused for several days.

## Kubernetes Cluster Upgrade Process
Before initiating the upgrade process, please schedule a specific time window for the upgrade since CI jobs could be impacted as pods are rescheduled to new nodes. Notify by sending an email to the Wikitech-l mailing list about the planned upgrade window. Include the estimated timeline. 

Please note: Each upgrade takes approximately 30 minutes in case of incremental upgrade process, so planning accordingly is advised.

Important Reminders:

Adjacent Version Upgrades: Remember that Kubernetes only supports upgrading between adjacent versions (for example, from 1.26.x to 1.27.y, but not from 1.26.x to 1.28.y). This policy necessitates a step-by-step approach for version upgrades spanning multiple releases.

Review Release Notes: Before upgrading, visit the DigitalOcean Kubernetes Changelog https://docs.digitalocean.com/products/kubernetes/details/changelog/ to read the release notes for the new version(s). It is crucial to follow the "Urgent Upgrade Notes" link, typically found in the notes for the first release of the major version you are upgrading to. These documents contain vital information that could affect your upgrade process and cluster operation.

## How to upgrade kubernetes cluster on DigitalOcean Console 
1. Notify in #wikimedia-releng channel on IRC with an appropriate !log message, e.g., `!log Starting gitlab cloud runners k8s cluster upgrade (T359594)`
1. Visit https://cloud.digitalocean.com/projects/ and select the cluster you want to upgrade.
1. Click on the cluster to view its details and then look for the available versions to upgrade.
1. Click "Upgrade Cluster" to initiate the upgrade process.
1. Wait until all the node pools' statuses show as "Running" in the overview section. This indicates that the upgrade is complete and the cluster is operational.
1. Confirm the cluster's connectivity and successful upgrade by running Kubernetes commands, e.g., `kubectl cluster-info` or`kubectl version`.
1. After the upgrade is complete, please update the `staging.tfvars` or `production.tfvars` file (as appropriate) to match the new Kubernetes version applied to the cluster. This step ensures the Terraform configuration accurately reflects the current state of the infrastructure.
1. Notify in #wikimedia-releng channel on IRC about the process completion, e.g, `!log kubernetes cluster upgrade process completed successfully (T359594)`

## How to upgrade the kubernetes cluster using terminal
To upgrade Kubernetes cluster using the terminal, you can use the DigitalOcean command-line tool (doctl). 
1. Notify in #wikimedia-releng channel on IRC with an appropriate !log message, e.g., `!log Starting gitlab cloud runners k8s cluster upgrade (T359594)`
1. First, ensure `doctl` is installed and configured. If not, refer tohttps://docs.digitalocean.com/reference/doctl/how-to/install/ for installation instructions.
1. List the available Kubernetes versions to find the one you want to upgrade to: `doctl kubernetes options versions`
1. Identify cluster NAME by listing Kubernetes clusters: `doctl kubernetes cluster list`
1. Initiate the upgrade by specifying the cluster name and the target version:  `doctl kubernetes cluster upgrade <cluster-name> --version <target-version>`
   Replace <cluster-name> with cluster's NAME and <target-version> with the desired Kubernetes version (e.g., 1.29.x-do.0).
1. Monitor the upgrade process through the DigitalOcean console or by periodically checking cluster's node statuses with `kubectl get nodes`.
1. Additionally, utilize doctl to monitor the upgrade status using `doctl k8s cluster get <cluster-name> --format "Name,Version,Status"`
e.g, 
```
$ doctl k8s cluster get <cluster-name> --format "Name,Version,Status"
Name         Version         Status
cluster-name    1.26.13-do.0    upgrading
```
1. Notify in #wikimedia-releng channel on IRC about the process completion, e.g, `!log kubernetes cluster upgrade process completed successfully (T359594)`

## How to force unlock Terraform state

There are instances where the Terraform state lock may remain in place even
though nothing is current running, for example if a pipeline is canceled. In
this case you're likely to see the following error message in jobs.

```
╷
│ Error: Error acquiring the state lock
│ 
│ Error message: HTTP remote state already locked:
│ ID=<uuid>
```

To fix this issue:

1. Manually run the pipeline against the `main` branch.
2. Define either a variable `TF_FORCE_UNLOCK_STAGING` or
   `TF_FORCE_UNLOCK_PRODUCTION` depending on whether the lock error was
   encountered in a staging or production related job. 
3. Set the variable value to the `<uuid>` string in the encountered error
   message (see the above example error).
4. Verify that the `force-unlock-staging` and/or the `force-unlock-production`
   jobs succeeded.

## How to force replacement of resources

Terraform does not always know the inner details of its managed resources and
whether a replacement is in order. This is particularly true of `helm_release`
resources. If ever you need to forcibly replace a resource, you can do so by:

1. Manually run the pipeline against the `production` branch.
2. Define the variable `TF_PLAN_APPLY_ARGS`.
3. Set the variable value to `--replace=<resource>` where
   `<resource>` is the fully qualified name of the resource that you want to
   forcibly replace.
   - Example: `--replace=module.docker-hub-mirror.helm_release.docker-hub-mirror`
4. Verfiy the output of "validate" jobs for the intended replacements.

## Monitoring
### Grafana access

Visit https://grafana.cloud.releng.team or https://grafana.staging.cloud.releng.team

#### Admin access

The password for the `admin` user is stored in the Release Engineering
1Password database.  There is a separate password for staging and
production.  If you have access to the Kubernetes cluster you can also
retrieve the admin password by running
```
kubectl -n monitoring get secret kube-prometheus-stack-grafana -o jsonpath={.data.admin-password} | base64 -d; echo
```

#### Setting/changing the Grafana admin password
* Discard the old Secret:
```
kubectl -n monitoring delete secret kube-prometheus-stack-grafana
```
* Create a new Secret with the new password:
```
kubectl -n monitoring create secret generic kube-prometheus-stack-grafana --from-literal=admin-user=admin --from-literal=admin-password=WHATEVER
```
* Update the information in the Release Engineering 1Password database.
* Kill any existing `kube-prometheus-stack-grafana-*` pod in the `monitoring` namespace to cause Grafana to restart and retrieve the new password information.
