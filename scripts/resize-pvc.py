#!/usr/bin/env python3

import argparse
import json
import subprocess


def kubectl(args: list, **kwargs):
    return subprocess.run(
        ["kubectl", "-n", "gitlab-runner"] + args,
        check=True,
        text=True,
        **kwargs,
    )


class PVC:
    def __init__(self, name, requested_size, current_size):
        self.name = name
        self.requested_size = requested_size
        self.current_size = current_size

    def __str__(self):
        return f"{self.name}, Req: {self.requested_size}, Cur: {self.current_size}"

    def resize(self, size: str):
        patch = {"spec": {"resources": {"requests": {"storage": size}}}}
        kubectl(["patch", "pvc", self.name, "--patch", json.dumps(patch)])


def get_pvcs(name) -> list[PVC]:
    """
    Return information about PVCs with label app.kubernetes.io/name=`name`.
    """

    p = kubectl(["config", "current-context"], stdout=subprocess.PIPE)
    print(f'Using kubectl context "{p.stdout.strip()}"\n')

    res = []

    p = kubectl(
        [
            "get",
            "pvc",
            "--selector",
            f"app.kubernetes.io/name={name}",
            "-o",
            "json",
        ],
        stdout=subprocess.PIPE,
    )
    for item in json.loads(p.stdout)["items"]:
        res.append(
            PVC(
                item["metadata"]["name"],
                item["spec"]["resources"]["requests"]["storage"],
                item["status"]["capacity"]["storage"],
            )
        )

    return res


def main():
    ap = argparse.ArgumentParser()

    ap.add_argument(
        "statefulset",
        help="The name of the statefulset whose PVCs should be resized. "
        + "For example, buildkitd or docker-hub-mirror.",
    )
    ap.add_argument("size", help="The new PVC size. For example, 20Gi")

    args = ap.parse_args()

    pvcs = get_pvcs(args.statefulset)

    print("The following PVCs will be affected:")
    for pvc in pvcs:
        print(pvc)

    answer = input(f"Resize these PVCs to {args.size}? ")
    if answer != "y":
        raise SystemExit("Cancelled")

    for pvc in pvcs:
        pvc.resize(args.size)

    print("Resize requests submitted.  It may take a while for the requests to be fulfilled.")


if __name__ == "__main__":
    main()
