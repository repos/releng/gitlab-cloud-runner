#!/usr/bin/env python3

import os
import json
import logging
import subprocess
import sys


# This script exits with 0 to prevent pipeline disruption, allowing subsequent
# steps to proceed even if errors occur.


def get_output(cmd):
    return subprocess.check_output(cmd, text=True)


def get_terraform_outputs() -> dict:
    terraform_command = os.environ.get("TERRAFORM", "gitlab-terraform")

    outputs = json.loads(get_output([terraform_command, "output", "-json"]))
    res = {}
    for key, value in outputs.items():
        res[key] = value["value"]
    return res


class State:
    def __init__(self, logger):
        self.logger = logger

        outputs = get_terraform_outputs()

        kubeconfig = outputs.get("kubeconfig")
        if not kubeconfig:
            self.bail("Kubeconfig is not defined in terraform state.")

        KUBECONFIG_PATH = "kubeconfig"
        with open(KUBECONFIG_PATH, "w") as f:
            f.write(kubeconfig)
        os.chmod(KUBECONFIG_PATH, 0o600)

        os.environ["KUBECONFIG"] = KUBECONFIG_PATH

    def bail(self, message):
        self.logger.error(message)
        sys.exit(0)

    def validate_cluster(self):
        # kubectl cluster-info can fail if the kubeconfig stored in
        # terraform state is out of date (which can happen if a
        # deployment hasn't occurred in a while.... DigitalOcean's
        # managed k8s rotates the kubeconfig auth on a regular basis).
        proc = subprocess.run(
            ["kubectl", "cluster-info"],
            text=True,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        if proc.returncode == 0 and "Kubernetes control plane" in proc.stdout:
            return

        if "You must be logged in to the server (Unauthorized)" in proc.stdout:
            self.bail("KUBECONFIG auth settings from terraform state are out of date.  Exiting.")

        self.bail(
            f"kubectl cluster-info exited with status {proc.returncode}\nOutput was:\n{proc.stdout}"
        )

    def fix_releases(self):
        """
        Fix any releases in a pending-* state
        """

        # status_map maps the helm release state to the helm command
        # to use to recover from that state.
        status_map = {
            "pending-install": "uninstall",
            "pending-upgrade": "rollback",
            "pending-rollback": "rollback",
        }

        for entry in json.loads(get_output(["helm", "list", "-A", "-a", "--output", "json"])):
            name = entry["name"]
            status = entry["status"]
            namespace = entry["namespace"]

            recovery_command = status_map.get(status)
            if recovery_command:
                self.logger.info(
                    f"Release {namespace}/{name} is in {status} state. "
                    f"Attempting to {recovery_command}..."
                )
                subprocess.run(["helm", recovery_command, name, "-n", namespace], check=True)


def main():
    logging.basicConfig(level=logging.INFO, format="%(message)s")
    logger = logging.getLogger(__name__)

    program = sys.argv[0]

    logger.info(f"{program} started.")
    try:
        state = State(logger)
        state.validate_cluster()
        state.fix_releases()
    finally:
        logger.info(f"{program} terminated.")


if __name__ == "__main__":
    main()
