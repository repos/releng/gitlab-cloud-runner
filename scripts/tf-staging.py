#!/usr/bin/env python3

import gitlab
import os
import subprocess
import sys


def main():
    gitlab_url = "https://gitlab.wikimedia.org"
    tf_state_name = "cloud-runner-staging"

    env = os.environ.copy()

    private_token = env.get("GITLAB_ACCESS_TOKEN")
    if not private_token:
        raise SystemExit("Please set GITLAB_ACCESS_TOKEN environment variable")

    user = env.get("USER")
    if not user:
        raise SystemExit("USER not set in environment")

    gl = gitlab.Gitlab(gitlab_url, private_token=private_token)
    project = gl.projects.get("repos/releng/gitlab-cloud-runner")

    # Add CI variables to the environment
    for var in project.variables.list(iterator=True):
        if var.variable_type != "env_var" or var.environment_scope not in [
            "DO/*",
            "DO/staging",
        ]:
            continue
        env[var.key] = var.value

    # Do some fixing up
    env["TF_VAR_do_token"] = env["DIGITALOCEAN_TOKEN"]

    # And deal with some outlier variables that set in .gitlab-ci.yml.
    # FIXME: Make it so that this is not needed.
    env["TF_VAR_runner_token"] = env["RUNNER_TOKEN_STAGING"]
    env["TF_VAR_runner_token_mem_optimized"] = env["RUNNER_TOKEN_STAGING_MEM_OPTIMIZED"]

    # Don't pass on GITLAB_ACCESS_TOKEN
    del env["GITLAB_ACCESS_TOKEN"]

    tf_state_url = f"{gitlab_url}/api/v4/projects/{project.id}/terraform/state/{tf_state_name}"

    # Run this quietly to avoid generating extra output (which may interfere with the
    # callers expectations)
    p = subprocess.run(
        [
            "terraform",
            "init",
            "-reconfigure",
            f"-backend-config=address={tf_state_url}",
            f"-backend-config=lock_address={tf_state_url}/lock",
            f"-backend-config=unlock_address={tf_state_url}/lock",
            f"-backend-config=username={user}",
            f"-backend-config=password={private_token}",
            "-backend-config=lock_method=POST",
            "-backend-config=unlock_method=DELETE",
            "-backend-config=retry_wait_min=5",
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        stdin=subprocess.DEVNULL,
    )
    if p.returncode != 0:
        raise SystemExit(f"terraform init failed:\n{p.stdout}")

    cmd = ["terraform"] + sys.argv[1:]

    if len(sys.argv) > 1 and sys.argv[1] in ["refresh", "plan", "apply", "destroy"]:
        cmd.append("-var-file=staging/staging.tfvars")

    p = subprocess.run(cmd, env=env)
    sys.exit(p.returncode)


if __name__ == "__main__":
    main()
