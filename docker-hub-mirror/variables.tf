variable "docker_hub_mirror_fqdn" {
  type = string
}

variable "namespace" {
  type = string
}

variable "docker_hub_user" {
  type = string
}

variable "docker_hub_password" {
  type = string
  sensitive = true
}

variable "docker_hub_mirror_secret" {
  description = "The name of the Kubernetes Secret resource which will hold Docker Hub credentials"
  type = string
  default = "docker-hub-mirror-creds"
}

variable "pvc_size" {
  type = string
}

variable "dependencies" {
  type = list
}
