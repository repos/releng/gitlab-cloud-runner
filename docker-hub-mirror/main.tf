resource "helm_release" "docker-hub-mirror" {
  chart     = "https://gitlab.wikimedia.org/repos/releng/docker-hub-mirror/-/jobs/artifacts/main/raw/helm/charts/docker-hub-mirror.tgz?job=package-and-publish-helm-chart"
  version   = "1.7.0"
  name      = "docker-hub-mirror"
  namespace = var.namespace

  values = [
    templatefile("${path.module}/docker-hub-mirror-values.yaml.tftpl",
      {
        pvc_size = var.pvc_size,
        docker_hub_mirror_fqdn = var.docker_hub_mirror_fqdn,
        docker_hub_mirror_secret = kubernetes_secret.docker-hub-mirror-creds.metadata[0].name,
        # This value is not used by the template.  It is just here
        # to establish dependencies within Terraform.
        dependencies = var.dependencies
      }
      )
  ]
}

resource "kubernetes_secret" "docker-hub-mirror-creds" {
  metadata {
    name = var.docker_hub_mirror_secret
    namespace = var.namespace
  }

  data = {
    username = var.docker_hub_user
    password = var.docker_hub_password
  }
}
