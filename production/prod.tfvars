# Node types/sizes for each pool
#
# DO cannot change instance size for existing node pools:
# See https://github.com/digitalocean/terraform-provider-digitalocean/issues/558
# Must run terraform destroy before deploying when changing this value.
node_pool_general_type = "g-2vcpu-8gb"
node_pool_memory_optimized_type = "g-8vcpu-32gb"

# Service resource requests and limits, and other settings
buildkitd_tag = "wmf-v0.20.0-2"
buildkitd_pvc_enabled = true
buildkitd_pvc_size = "40Gi"
buildkitd_gckeepstorage = 10
buildkitd_cpu_request = "1500m"
buildkitd_memory_request = "9Gi"
buildkitd_memory_limit = "9Gi"

docker_hub_mirror_pvc_size = "50Gi"

reggie_cpu_request = "1800m"
reggie_ephemeral_storage_request = "30Gi"
reggie_memory_request = "2Gi"
reggie_memory_limit = "4Gi"

runner_cpuopt_cpu_request = "250m"
runner_cpuopt_cpu_limit = ""
runner_cpuopt_memory_request = "1Gi"
runner_cpuopt_memory_limit = "1Gi"

# The goal is to allow 3 jobs to run comfortably on a memory-
# optimized node.

# g-8vcpu-32gb has 7900m allocatable CPU.
# System pods request about 510m.  Let's say that 7200M cpu is available for runners.
# Per runner overhead:
# istio-proxy adds cpu request of 100m and mem of 128Mi.
# the helper  adds cpu request of 100m and mem of 512Mi (See ../gitlab/gitlab-runner-values.yaml.tftpl)
# 7200/3 = 2400 cpu available per job.  Removing 200m for overhead and
# another 100m for margin, land on 2100m request.
runner_memopt_cpu_request = "2100m"
runner_memopt_cpu_limit = ""

# g-8vcpu-32gb has 29222Mi allocatable memory.
# buildkitds get distributed among the nodes, so assume 10Gi (buildkit'd mem limit) is used
# on all nodes, so let's say 19222Mi allocatable.
# Per runner overhead:
# istio-proxy adds cpu request of 100m and mem of 128Mi.
# the helper  adds cpu request of 100m and mem of 512Mi (See ../gitlab/gitlab-runner-values.yaml.tftpl)
# 19222/3 = ~6400Mi mem per job. Reducing by 700Mi for overhead,
# land on 5700Mi request/limit.
runner_memopt_memory_request = "5700Mi"
runner_memopt_memory_limit = "5700Mi"

kubernetes_version = "1.30.10-do.0"
