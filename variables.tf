variable "project" {
  description = "The project name under which to create resources"
  type        = string
  default     = "gitlab-cloud-runner"
}

variable "do_token" {
  description = "A DigitalOcean API token with read/write access"
  type        = string
  sensitive   = true
}

variable "access_id" {
  description = "The access key ID used for Spaces API operations."
}

variable "secret_key" {
  description = "The secret access key used for Spaces API operations."
  type        = string
  sensitive   = true
}

variable "cache_bucket" {
  description = "Bucket used for caches"
  type        = string
  default     = "org-wikimedia-releng-cloud-runner-cache"
}

variable "bucket_region" {
  description = "Region of the spaces storage bucket"
  type        = string
  default     = "nyc3"
}

variable "cluster_region" {
  description = "Region of Kubernetes cluster"
  type        = string
  default     = "nyc3"
}

variable "cluster_ha" {
  description = "Switch to enable high available control plane"
  type        = bool
  default     = false
}

variable "cluster_auto_upgrade" {
  description = "Switch to enable automatic patch release upgrades"
  type        = bool
  default     = false
}

variable "cluster_name" {
  description = "name for the cluster"
  type        = string
  default     = "cloud-runner"
}

variable "cluster_domain" {
  description = "Domain name for the cluster"
  type        = string
  default     = "cloud.releng.team"
}

variable "container_registry_hostname" {
  description = "Host name of reggie's load balancer"
  type        = string
  default     = "registry"
}

variable "docker_hub_mirror_hostname" {
  description = "Host name of the docker hub mirror"
  type        = string
  default     = "docker-hub-mirror"
}

variable "docker_hub_mirror_pvc_size" {
  description = "The size of the PVC used by docker hub mirror"
  type        = string
}

variable "docker_hub_user" {
  description = "The username to use when docker-hub-mirror authenticates with Docker Hub"
  type        = string
}

variable "docker_hub_password" {
  description = "The password to use when docker-hub-mirror authenticates with Docker Hub"
  type        = string
  sensitive   = true
}

variable "ingress_cluster_ip" {
  description = "The statically assigned ClusterIP for our ingress service"
  type        = string
  default     = "10.245.30.9"
}

variable "node_pool_min_nodes" {
  description = "Minimum number of machine size in each Kubernetes node pool"
  type        = number
  default     = 4
}

variable "node_pool_max_nodes" {
  description = "Maximum number of machine size in each Kubernetes node pool"
  type        = number
  default     = 20
}

variable "node_pool_general_type" {
  description = "Instance type used for general purpose nodes"
  type        = string
}

variable "node_pool_memory_optimized_type" {
  description = "Instance type used for memory optimized nodes"
  type        = string
}

variable "runner_poll_timeout" {
  description = "The number of seconds to wait on new runner pods before timing out"
  type        = number
  default     = 1200
}

variable "runner_token" {
  description = "The token to register the runner with Gitlab"
  type        = string
  sensitive   = true
}

variable "runner_token_mem_optimized" {
  description = "The token to register the memory optimized runner with Gitlab"
  type        = string
  sensitive   = true
}

# Resource request/limit tuning, and other settings.
# Values should be set per environment in either production/prod.tfvars or staging/staging.tfvars
variable "buildkitd_tag" {
  type        = string
  description = "The tag of the buildkitd image to use"
  default     = "" # Blank means use the tag specified in the helm chart
}

variable "buildkitd_max_connections" {
  description = "Maximum number of concurrent connections to each buildkitd instance"
  type        = number
  default     = 10
}

variable "buildkitd_gckeepstorage" {
  type        = number
  description = "BuildkitD cache target storage size in GB"
}

variable "buildkitd_pvc_enabled" {
  type        = bool
}

variable "buildkitd_pvc_size" {
  type        = string
}

variable "buildkitd_cpu_request" {
  type        = string
}

variable "buildkitd_memory_request" {
  type        = string
}

variable "buildkitd_memory_limit" {
  type        = string
}

variable "reggie_cpu_request" {
  type        = string
}

variable "reggie_memory_request" {
  type        = string
}

variable "reggie_ephemeral_storage_request" {
  type        = string
}

variable "reggie_memory_limit" {
  type        = string
}

variable "runner_cpuopt_cpu_request" {
  type        = string
}

variable "runner_cpuopt_cpu_limit" {
  type        = string
}

variable "runner_cpuopt_memory_request" {
  type        = string
}

variable "runner_cpuopt_memory_limit" {
  type        = string
}

variable "runner_memopt_cpu_request" {
  type        = string
}

variable "runner_memopt_cpu_limit" {
  type        = string
}

variable "runner_memopt_memory_request" {
  type        = string
}

variable "runner_memopt_memory_limit" {
  type        = string
}

variable "kubernetes_version" {
   description = "The kubernetes version in use."
   type        = string
}
