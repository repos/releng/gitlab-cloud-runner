include:
  - project: 'repos/releng/kokkuri'
    file: 'includes/images.yaml'

variables:
  TF_ROOT: ${CI_PROJECT_DIR}
  TF_STATE_NAME: cloud-runner-2

  # Additional arguments to pass to terraform plan and terraform apply
  TF_PLAN_APPLY_ARGS:

  # In the case where terraform was interrupted (e.g. pipeline was cancelled)
  # and its lock remains in place, you can pass the lock id via this variable
  # to force the unlocking of it. The lock ID can be found in the error
  # message. For example:
  #   | Error: Error acquiring the state lock
  #   │ 
  #   │ Error message: HTTP remote state already locked: ID=<uuid>
  #
  # TF_FORCE_UNLOCK_PRODUCTION: <uuid>
  # TF_FORCE_UNLOCK_STAGING: <uuid>

  # Variables available for debugging
  DEBUG: ""

default:
  before_script:
    - |
      [[ "$DEBUG" ]] && set -o xtrace

stages:
  - prepare
  - unlock
  - destroy
  - test
  - deploy

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == 'production' && $CI_COMMIT_REF_PROTECTED

lint:
  stage: prepare
  image: docker-registry.wikimedia.org/python3-devel:0.0.3-20231106-20231106
  script: |
    apt-get update && apt-get install -y git flake8 black
    set -x
    flake8 --max-line-length 100
    black --check --diff -l 100 .

test-deployer-build:
  stage: prepare
  extends: .kokkuri:build-image
  variables:
    BUILD_VARIANT: deployer
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

terraform-init:
  image: docker-registry.wikimedia.org/repos/releng/gitlab-terraform-images:wmf-v0.45.1-4
  stage: prepare
  script:
    - gitlab-terraform init -reconfigure
    - gitlab-terraform validate
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

build-deployer:
  stage: prepare
  extends: .kokkuri:build-and-publish-image
  variables:
    BUILD_VARIANT: deployer
    PUBLISH_IMAGE_NAME: '${CI_PROJECT_PATH}/deployer'
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
    - if: $CI_COMMIT_BRANCH == "production" && $CI_COMMIT_REF_PROTECTED
  tags:
    - trusted

.validate-plan:
  image: ${BUILD_DEPLOYER_IMAGE_REF}
  stage: test
  script:
    - !reference [terraform-init, script]
    - echo "Validating terraform plan"
    - gitlab-terraform plan $ARGS $TF_PLAN_APPLY_ARGS
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
    - if: $CI_COMMIT_BRANCH == "production" && $CI_COMMIT_REF_PROTECTED
  tags:
    - trusted

validate-plan-staging:
  extends: .validate-plan
  variables:
    TF_STATE_NAME: cloud-runner-staging
    TF_VAR_runner_token: $RUNNER_TOKEN_STAGING
    TF_VAR_runner_token_mem_optimized: $RUNNER_TOKEN_STAGING_MEM_OPTIMIZED
    ARGS: -var-file=staging/staging.tfvars
  environment:
    name: DO/staging

validate-plan-production:
  extends: .validate-plan
  variables:
    ARGS: -var-file=production/prod.tfvars
  environment:
    name: DO/production

push-to-production:
  image: docker-registry.wikimedia.org/wmfdebug
  stage: deploy
  environment:
    name: DO/production
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
      when: manual
  before_script:
    - git remote set-url origin https://gitlab-ci-token:${DEPLOY_TOKEN}@gitlab.wikimedia.org/${CI_PROJECT_PATH}.git
  script:
    - git fetch origin ${CI_COMMIT_BRANCH}
    - git push origin +FETCH_HEAD:production
  tags:
    - trusted

.force-unlock:
  image: ${BUILD_DEPLOYER_IMAGE_REF}
  stage: unlock
  script:
    - !reference [terraform-init, script]
    - gitlab-terraform force-unlock -force $FORCE_UNLOCK_UUID
  tags:
    - trusted

force-unlock-staging:
  extends: .force-unlock
  variables:
    TF_STATE_NAME: cloud-runner-staging
    FORCE_UNLOCK_UUID: $TF_FORCE_UNLOCK_STAGING
  rules:
    - if: $TF_FORCE_UNLOCK_STAGING

force-unlock-production:
  extends: .force-unlock
  variables:
    FORCE_UNLOCK_UUID: $TF_FORCE_UNLOCK_PRODUCTION
  rules:
    - if: $TF_FORCE_UNLOCK_PRODUCTION

.deploy:
  image: ${BUILD_DEPLOYER_IMAGE_REF}
  stage: deploy
  script:
    - !reference [terraform-init, script]
    - gitlab-terraform plan $ARGS $TF_PLAN_APPLY_ARGS
    - scripts/helm-check.py
    - gitlab-terraform apply $TF_PLAN_APPLY_ARGS
  tags:
    - trusted

deploy-production:
  extends: .deploy
  variables:
    ARGS: -var-file=production/prod.tfvars
  environment:
    name: DO/production
  rules:
    - if: $CI_COMMIT_BRANCH == "production" && $CI_COMMIT_REF_PROTECTED

deploy-staging:
  extends: .deploy
  variables:
    TF_STATE_NAME: cloud-runner-staging
    TF_VAR_runner_token: $RUNNER_TOKEN_STAGING
    TF_VAR_runner_token_mem_optimized: $RUNNER_TOKEN_STAGING_MEM_OPTIMIZED
    ARGS: -var-file=staging/staging.tfvars
  environment:
    name: DO/staging
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
      when: manual

# The CONFIRM_DESTROY variable must be set to the project name (gitlab-cloud-runner) when running
# the pipeline for this job to be available for manual triggering
.destroy:
  image: ${BUILD_DEPLOYER_IMAGE_REF}
  stage: destroy
  script:
    - !reference [terraform-init, script]
    - gitlab-terraform destroy $ARGS
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED) && $CONFIRM_DESTROY == $CI_PROJECT_NAME
      when: manual
  tags:
    - trusted

destroy-staging:
  extends: .destroy
  variables:
    TF_STATE_NAME: cloud-runner-staging
    TF_VAR_runner_token: $RUNNER_TOKEN_STAGING
    TF_VAR_runner_token_mem_optimized: $RUNNER_TOKEN_STAGING_MEM_OPTIMIZED
    ARGS: -var-file=staging/staging.tfvars
  environment:
    name: DO/staging

destroy-production:
  extends: .destroy
  variables:
    ARGS: -var-file=production/prod.tfvars
  environment:
    name: DO/production
