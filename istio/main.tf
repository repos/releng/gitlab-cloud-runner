# The namespace for the istio service mesh
resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "istio-system"
    }

    name = "istio-system"
  }
}

# This chart installs resources shared by all Istio revisions. This includes Istio CRDs.
resource "helm_release" "istio-base" {
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart      = "base"
  name       = "istio-base"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "1.16.1"
}

# This chart installs an Istiod deployment.
resource "helm_release" "istiod" {
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart      = "istiod"
  name       = "istiod"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = helm_release.istio-base.version
}

output "depend" {
  value = helm_release.istiod.metadata[0].name
  description = "This output is to allow for establishing dependencies"
}
