variable "cache_bucket" {
  description = "Bucket used for caches"
  type        = string
}

variable "bucket_region" {
  description = "Region of the spaces storage bucket"
  type        = string
}

variable "runner_poll_timeout" {
  description = "The number of seconds to wait on new runner pods before timing out"
  type        = number
}

variable "concurrent" {
  description = "The number of concurrent jobs that can be scheduled by the runner"
  type        = number
  default     = 40
}

variable "replicas" {
  description = "The number of runner pods in the replicaset (overall capacity is this * concurrency)"
  type        = number
  default     = 1
}

variable "check_interval" {
  description = "The frequency (in seconds) at which runners will poll for new jobs"
  type        = number
  default     = 3
}

# Note that request_concurrency, along with check_interval and concurrent,
# will affect the overall latency of CI job scheduling.
#
# See https://gitlab.com/gitlab-org/charts/gitlab-runner/-/issues/148#note_304636866
#   max_scheduling_latency = (concurrent / request_concurrency) * check_interval
#
# However, this is an imperfect formula since each worker that grabs a new job
# signals other workers to check immediately. Just keep request_concurrency
# and check_interval at values enough to grab new jobs quickly, but not to
# overwhelm the server.
variable "request_concurrency" {
  description = "The number of requests for new jobs that a runner can make concurrently"
  type        = number
  default     = 4
}

variable "container_registry_fqdn" {
  type = string
}

variable "runner_token" {
  description = "The token to register the runner with Gitlab"
  type        = string
  sensitive   = true
}

variable "runner_token_mem_optimized" {
  description = "The token to register the memory optimized runner with Gitlab"
  type        = string
  sensitive   = true
}

# CPU-optimized resource requests/limits
variable "cpuopt_cpu_request" {
  description = "K8s CPU request for CPU-optimized runners"
  type        = string
}

variable "cpuopt_cpu_limit" {
  description = "K8s CPU limit for CPU-optimized runners"
  type        = string
  default     = ""
}

variable "cpuopt_memory_request" {
  description = "K8s memory request for CPU-optimized runners"
  type        = string
}

variable "cpuopt_memory_limit" {
  description = "K8s memory limit for CPU-optimized runners"
  type        = string
}

# memory-optimized resource requests/limits
variable "memopt_cpu_request" {
  description = "K8s CPU request for memory-optimized runners"
  type        = string
}

variable "memopt_cpu_limit" {
  description = "K8s CPU limit for memory-optimized runners"
  type        = string
  default     = ""
}

variable "memopt_memory_request" {
  description = "K8s memory request for memory-optimized runners"
  type        = string
}

variable "memopt_memory_limit" {
  description = "K8s memory limit for memory-optimized runners"
  type        = string
}
