# The namespace where the runner, executors, and buildkitd reside
resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "gitlab-runner"
    }

    labels = {
      istio-injection = "enabled"
    }

    name = "gitlab-runner"
  }
}

resource "helm_release" "this" {
  chart      = "gitlab-runner"
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "0.47.1"
  timeout    = 180

  values = [
    templatefile("${path.module}/gitlab-runner-values.yaml.tftpl", {
      bucket_region           = var.bucket_region
      cache_bucket            = var.cache_bucket
      check_interval          = var.check_interval
      concurrent              = var.concurrent
      container_registry_fqdn = var.container_registry_fqdn
      replicas                = var.replicas
      request_concurrency     = var.request_concurrency
      runner_poll_timeout     = var.runner_poll_timeout
      workload                = "memory"
      cpu_request             = var.cpuopt_cpu_request
      cpu_limit               = var.cpuopt_cpu_limit
      memory_request          = var.cpuopt_memory_request
      memory_limit            = var.cpuopt_memory_limit
    })
  ]

  set_sensitive {
    name  = "runnerToken"
    value = var.runner_token
  }
}

resource "helm_release" "runner_memopt" {
  chart      = "gitlab-runner"
  name       = "gitlab-runner-memopt"
  repository = "https://charts.gitlab.io"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "0.47.1"
  timeout    = 180

  values = [
    templatefile("${path.module}/gitlab-runner-values.yaml.tftpl", {
      bucket_region           = var.bucket_region
      cache_bucket            = var.cache_bucket
      check_interval          = var.check_interval
      concurrent              = var.concurrent
      container_registry_fqdn = var.container_registry_fqdn
      replicas                = var.replicas
      request_concurrency     = var.request_concurrency
      runner_poll_timeout     = var.runner_poll_timeout
      workload                = "memory"
      cpu_request             = var.memopt_cpu_request
      cpu_limit               = var.memopt_cpu_limit
      memory_request          = var.memopt_memory_request
      memory_limit            = var.memopt_memory_limit
    })
  ]

  set_sensitive {
    name  = "runnerToken"
    value = var.runner_token_mem_optimized
  }
}

resource "kubernetes_deployment" "gitlab-runner-pod-cleanup" {
  metadata {
    name = "gitlab-runner-pod-cleanup"
    namespace = kubernetes_namespace.this.metadata[0].name
  }

  spec {
    selector {
      match_labels = {
        app = "gitlab-runner-pod-cleanup"
      }
    }

    template {
      metadata {
        labels = {
          app = "gitlab-runner-pod-cleanup"
        }
      }

      spec {
        # Use the serviceaccount that is created by the gitlab-runner chart.
        service_account_name = "gitlab-runner"
        container {
          image = "registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:latest"
          name = "gitlab-runner-pod-cleanup"
          env {
            name = "POD_CLEANUP_KUBERNETES_NAMESPACES"
            value = kubernetes_namespace.this.metadata[0].name
          }
        }
      }
    }
  }
}
