module "do" {
  source = "./digitalocean"

  # We must propagate all variables explicitly to the child module
  project                          = var.project
  access_id                        = var.access_id
  bucket_region                    = var.bucket_region
  cache_bucket                     = var.cache_bucket
  cluster_auto_upgrade             = var.cluster_auto_upgrade
  cluster_name                     = var.cluster_name
  cluster_domain                   = var.cluster_domain
  cluster_ha                       = var.cluster_ha
  cluster_region                   = var.cluster_region
  container_registry_hostname      = var.container_registry_hostname
  node_pool_max_nodes              = var.node_pool_max_nodes
  node_pool_min_nodes              = var.node_pool_min_nodes
  node_pool_general_type           = var.node_pool_general_type
  node_pool_memory_optimized_type  = var.node_pool_memory_optimized_type
  secret_key                       = var.secret_key
  kubernetes_version               = var.kubernetes_version 
}

module "certs" {
  source = "./certs"
}

module "istio" {
  source = "./istio"
}

module "monitoring" {
  source                 = "./monitoring"
  cluster_domain         = var.cluster_domain
}

module "ingress" {
  source                 = "./ingress"
  cluster_domain         = var.cluster_domain
  cluster_subnet         = module.do.cluster_subnet
  cluster_service_subnet = module.do.cluster_service_subnet
  service_cluster_ip     = var.ingress_cluster_ip
  # Ensure that the networking stack is stable before setting up the
  # ingress controller (which sets up a webhook that needs reliable connectivity).
  dependencies            = [ module.istio.depend, module.certs.depend ]
}

module "gitlab" {
  source                     = "./gitlab"
  cache_bucket               = var.cache_bucket
  bucket_region              = var.bucket_region
  container_registry_fqdn    = module.do.container_registry_fqdn
  runner_poll_timeout        = var.runner_poll_timeout
  runner_token               = var.runner_token
  runner_token_mem_optimized = var.runner_token_mem_optimized
  cpuopt_cpu_request         = var.runner_cpuopt_cpu_request
  cpuopt_cpu_limit           = var.runner_cpuopt_cpu_limit
  cpuopt_memory_request      = var.runner_cpuopt_memory_request
  cpuopt_memory_limit        = var.runner_cpuopt_memory_limit
  memopt_cpu_request         = var.runner_memopt_cpu_request
  memopt_cpu_limit           = var.runner_memopt_cpu_limit
  memopt_memory_request      = var.runner_memopt_memory_request
  memopt_memory_limit        = var.runner_memopt_memory_limit
}

module "buildkitd" {
  source                 = "./buildkitd"
  namespace              = module.gitlab.namespace
  buildkitd_tag          = var.buildkitd_tag
  max_connections        = var.buildkitd_max_connections
  docker_hub_mirror_fqdn = module.do.docker_hub_mirror_fqdn
  cpu_request            = var.buildkitd_cpu_request
  memory_request         = var.buildkitd_memory_request
  memory_limit           = var.buildkitd_memory_limit
  pvc_enabled            = var.buildkitd_pvc_enabled
  pvc_size               = var.buildkitd_pvc_size
  gckeepstorage          = var.buildkitd_gckeepstorage
}

module "reggie" {
  source                    = "./reggie"
  namespace                 = module.gitlab.namespace
  container_registry_fqdn   = module.do.container_registry_fqdn
  cpu_request               = var.reggie_cpu_request
  memory_request            = var.reggie_memory_request
  memory_limit              = var.reggie_memory_limit
  ephemeral_storage_request = var.reggie_ephemeral_storage_request 
  dependencies              = [ module.ingress.depend ]
}

module "k8s-pvc-cleaner" {
  source                  = "./k8s-pvc-cleaner"
  namespace               = module.gitlab.namespace
}

module "docker-hub-mirror" {
  source                 = "./docker-hub-mirror"
  namespace              = module.gitlab.namespace
  docker_hub_mirror_fqdn = module.do.docker_hub_mirror_fqdn
  docker_hub_user        = var.docker_hub_user
  docker_hub_password    = var.docker_hub_password
  pvc_size               = var.docker_hub_mirror_pvc_size
  dependencies            = [ module.ingress.depend ]
}

module "external-dns" {
  source                 = "./externaldns"
  namespace              = module.gitlab.namespace
  cluster_name           = var.cluster_name
  token                  = var.do_token
}

resource "kubernetes_config_map" "coredns-custom" {
  metadata {
    name      = "coredns-custom"
    namespace = "kube-system"
  }

  data = {
    "additional-hosts.override" = <<-EOT
    hosts {
      ${var.ingress_cluster_ip} ${module.do.container_registry_fqdn} ${module.do.docker_hub_mirror_fqdn}
      fallthrough
    }
    EOT
  }
}

output "namespace" {
  value = module.gitlab.namespace
  description = "The Kubernetes namespace used by gitlab runner."
}

output "kubeconfig" {
  value = module.do.kube_config
  description = "The raw kubeconfig content for the kubernetes cluster."
  sensitive = true
}  
