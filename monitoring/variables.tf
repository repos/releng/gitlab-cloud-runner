variable "cluster_domain" {
  description = "Domain name for the cluster"
  type        = string
}
