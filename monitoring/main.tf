# The namespace where the prometheus stack resides
resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = "monitoring"
    }

    name = "monitoring"
  }
}

resource "helm_release" "kube-prometheus-stack" {
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  name       = "kube-prometheus-stack"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "69.8.2"

  values = [
    file("${path.module}/values/prometheus-stack.yaml"),
    templatefile("${path.module}/values/grafana.yaml.tftpl", {
      cluster_domain = var.cluster_domain
    })
  ]
}

resource "helm_release" "prometheus-adapter" {
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-adapter"
  name       = "prometheus-adapter"
  namespace  = kubernetes_namespace.this.metadata[0].name
  version    = "3.4.2"

  values = [
    file("${path.module}/values/prometheus-adapter.yaml")
  ]
}
